#! /usr/bin/env lua
--
-- hello.lua
-- Copyright (C) 2015 Adrian Perez <aperez@igalia.com>
-- Modified by Sebastian Huebner <sh@kokolor.es>
--
-- Distributed under terms of the MIT license.
--
-- TODO
-- - loading greetings from config
-- - grumpy and friendly greetings

local hello_msg = {
  morning = {
    "Moin",
    "Guten Morgen, Sir.",
    "Guten Morgen, der Herr",
    "Guten Morgen, die Dame",
    "Guten Morgen mein Honigbärchen",
    "Moin Schnucki",
    "Ich hasse dich jetzt schon",
    "Moinsen",
    "Lass mich in Ruhe",
    "Morschn"
  },
  day = {
    "Da ist ja endlich mein Super Puper",
    "Lass mich in Ruhe",
    "Moinsen",
    "Heho",
    "Tach"
  },
  evening = {
    "Alter, mal auf die Uhr geschaut?",
    "Wie? Feierabend JUUUUUUUUUNGE!"
  }
}

local function hello(command)
  local hour = tonumber(os.date('%H'))
  local msg

  math.randomseed( os.time() )

  if hour < 11 then
    msg = hello_msg.morning[math.random(1, #hello_msg.morning)]
  elseif hour >= 11 and hour < 17 then
    msg = hello_msg.day[math.random(1, #hello_msg.day)]
  else
    msg = hello_msg.evening[math.random(1, #hello_msg.evening)]
  end

  return msg
end

local bye_msg = {
  early = {
    "Ey! Zu früh! Zurück an deinen Platz!",
    "Haste heute überhaupt schon was getan?",
    "Ahja, nenn halben Tag also.",
    "Ok, ich schreib nenn halben Tag auf."
  },
  right = {
    "Gehabt euch wohl",
    "Einen wunderbaren Abend wünsche ich",
    "Bis später mein Süßer",
    "Schööööö",
    "Adios meine kleine Zaubermaus",
    "Adios",
    "Endlich isser weg",
    "Ich wünschen Ihnen noch eine schönen Tag, Sir.",
    "Hau ab",
    "Wir sehen uns",
    "Bis morgen",
    "Hau rein!",
    "Fierovend",
    "Färdsch! Undnu machma Feiorahmd"
  },
  late = {
    "Mal auf die Uhr geguckt?",
    "Verpiss dir endlich!",
    "Feierabend JUUUUUUUUUNGE!",
    "Matsch heeme!",
    "Du hast schon lange Feierabend!",
    "Na nu aber Zack Zack!"
  }
}
local function goodbye(command)
  local hour = tonumber(os.date('%H'))
  local msg

  math.randomseed( os.time() )

  if hour < 16 then
    msg = bye_msg.early[math.random(1, #bye_msg.early)]
  elseif hour >= 17 and hour < 18 then
    msg = bye_msg.right[math.random(1, #bye_msg.right)]
  else
    msg = bye_msg.late[math.random(1, #bye_msg.late)]
  end

  return msg
end


return function (bot)
  bot:add_plugin("commandevent")
  bot:hook("command/hallo", hello)
  bot:hook("command/Hallo", hello)
  bot:hook("command/hi", hello)
  bot:hook("command/Hi", hello)
  bot:hook("command/moin", hello)
  bot:hook("command/Moin", hello)
  bot:hook("command/tschuess", goodbye)
  bot:hook("command/Tschuess", goodbye)
  bot:hook("command/bye", goodbye)
  bot:hook("command/Bye", goodbye)
end
