local urlfetch = require('util.urlfetch')
local json     = require('util.json')

local function a_joke(bot, command)
  local http_options = {
    method = 'GET',
    headers = {
      ['Accept'] = 'application/json'
    }
  }
  local urls = {
    'http://api.icndb.com/jokes/random?escape=javascript'
  }

  local url = math.random(1, #urls)

  urlfetch(urls[url], http_options, function(data, code, response)
    if code ~= 200 then
      bot:warn('jokes: HTTP code is %d: %s', code, urls[url])
      return
    end

    local content_type = response.headers['content-type']
    if content_type ~= 'application/json' then
      bot:warn('jokes: Wrong content-type: %s', content_type)
      return
    end

    local joke = json.decode(data)
    joke = joke and joke.type == 'success' and joke.value.joke

    if not joke then
      bot:warn('jokes: Can\'t get joke')
    end

    command:post(joke)
  end)

  return true
end

return function(bot)
  bot:add_plugin('commandevent')
  bot:hook('command/joke', function(cmd) return a_joke(bot, cmd) end)
end
